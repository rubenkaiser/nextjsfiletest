import Handlebars from 'handlebars';
import { readFileSync, readdirSync } from 'fs';
import path from 'path';

const emailsDir = path.resolve(process.cwd(), 'emails');

const sendMail = async (
  from,
  to,
  subject = 'Form submission',
  data = {},
  templateName = null,
) => {

  console.error('path', process.cwd());
  console.error('cwd', readdirSync(process.cwd()));
  console.error('emails', readdirSync(emailsDir));

  const emailFile = readFileSync(path.join(emailsDir, templateName || 'generic.html'), {
    encoding: 'utf8',
  });

  const emailTemplate = Handlebars.compile(emailFile);

  if (data.partialTemplateName) {
    const partialFile = readFileSync(path.join(emailsDir, data.partialTemplateName), {
      encoding: 'utf8',
    });
    Handlebars.registerPartial('partial', partialFile);
  }

  const message = {
    from,
    to,
    subject,
    html: emailTemplate({
      ...data,
    }),
  };

  console.log(message);

  return true;
};

export default sendMail;
