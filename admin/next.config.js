const withGraphql = require('./withGraphql');

const withTM = require('next-transpile-modules')([]);

module.exports = () => {
  return withGraphql(
    withTM({
      swcMinify: process.env.NODE_ENV === 'production',
      compress: process.env.NODE_ENV === 'production',
    })
  )
}
